# RD/MPCTools Support

Welcome to the support page for RD/MPCTools, the Extension Toolkit for Martin M-Series Software.

If you are new to RD/MPCTools, I recommend that you read the [manual](http://ricardo-dias.com/mpctools/manual/) to get you up and running.

In the [Repository](https://bitbucket.org/ricdays/mpctools-support/src), you will find mapping files for MIDI devices and LUA scripts.

In the [Wiki](https://bitbucket.org/ricdays/mpctools-support/wiki/), you will find instructions, function reference, tutorials, etc.

There's also a [discussion forum](http://forum.martin.com/c/martin-m-series/midi) to ask for help, share ideas, give feedback, etc.