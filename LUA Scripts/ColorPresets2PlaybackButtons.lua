-- 	Color Presets to Playback Buttons
-- 		Author: Ricardo Dias <ricdays@gmail.com>
--
-- This script creates a sequence of buttons in the playback button
-- grid for the selected group with preset name and appearance, and a 
-- single cue containing the group with the preset recorded

-------------------
-- Configuration --
-------------------

fixtureGroupNumber 		= 1		-- The fixture group to be used

presetNumberStart 		= 1		-- First color preset to be copied to playback buttons
presetNumberEnd 		= 2		-- Last color preset to be copied to playback buttons

playbackButtonPage 		= 1		-- Starting playback button page
playbackButtonStart 	= 11	-- First playback button

----------------------------------------------------
-- Main Script - dont change if you don't need to --
----------------------------------------------------

ClearProgrammer(); -- Make sure the programmer is cleared

counter = 1
for presetNumber = presetNumberStart, presetNumberEnd do

	-- Select fixtures and presets and record cuelist in the cuelist directory
	SelectGroup(fixtureGroupNumber)
	SelectColorPreset(presetNumber)
	cuelistName = GetColorPresetName(presetNumber)
	cuelistNumber = RecordCuelistOnDirectory(cuelistName)
	SetCuelistAppearance(cuelistNumber, GetColorPresetAppearance(presetNumber));
	
	print(cuelistName)
	
	ClearProgrammer()

	-- Assign cuelist to the playback button
	pbkBtnNum = playbackButtonStart - 1 + counter
	CopyCuelistFromDirectoryToPlaybackButton(cuelistNumber,playbackButtonPage,pbkBtnNum)

	counter = counter + 1
end

ClearProgrammer(); -- Make sure the programmer is cleared in the end