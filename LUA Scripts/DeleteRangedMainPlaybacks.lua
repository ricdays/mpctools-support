-- 	Delete Main Playbacks Looped
-- 		Author: Harley Morgan <hkmorgan87@hotmail.com>
--		Based on the original script "Delete Main Playbacks" from Stan Pruitt <stan@stanpruitt.com>
--
-- This script deletes a range of cuelists from the selected Main Playback Bank 

-------------------
-- Configuration --
-------------------
sourceBankNumber = 1
sourceBankNumEnd = 10
sourcePlaybackNumberStart = 1
sourcePlaybackNumberEnd = 20




----------------------------------------------------
-- Main Script - dont change if you don't need to --
----------------------------------------------------
ClearProgrammer(); -- Make sure the programmer is cleared

repeat
for
	playbackNumber = sourcePlaybackNumberStart, sourcePlaybackNumberEnd do
	DeleteMainPlayback(sourceBankNumber, playbackNumber)
		
end

sourceBankNumber = sourceBankNumber + 1
until(sourceBankNumber > sourceBankNumEnd)
	