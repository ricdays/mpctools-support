-- 	Auto Presets Generation
-- 		Author: Sylvain Guiblain <sylvain.guiblain@gmail.com> http://spb8.lighting
--
-- This script will create or remove empty presets for several types.
-- For each preset type, like Dimmer or Colors, the script will request it to you first.
-- As this script empty the programmer, please don't forget to record your programmer before to launch it ...

-------------------
-- Configuration --
-------------------

-- Color are in RGB

	Settings = {
		PresetGridWidth = 4,				-- Adjust the preset position following your Preset Grid width (min width is 4, less value = overlapping preset)
		PresetINTENSITYStartPosition = 1,	-- Adjust the start point from the first line for INTENSITY presets
		PresetCOLORStartPosition = 1,		-- Adjust the start point from the first line for COLOR presets
		PresetBEAMStartPosition = 1,		-- Adjust the start point from the first line for BEAM presets
		PlaybackButtonPage = 1,				-- Starting playback button page for creating cuelist from preset
		Language = "EN",					-- Language setting, can be set to "EN" for english or "FR" for French
		WaitTime = 0.5						-- Adjust this value depending of your hardware and the number of fixture. It is used to delay the creation of preset content, MPC doesn't like to be too quick. If the value is too low, the preset generation can be not correct
	}

	Spb8Intensities = {
		{name = "Dimmer 100%", 			color = "255, 255, 255",		value=255, 		position = Settings.PresetINTENSITYStartPosition},
		{name = "Dimmer 50%", 			color = "160, 160, 160", 		value=127,		position = Settings.PresetINTENSITYStartPosition+Settings.PresetGridWidth},
		{name = "Dimmer 0%", 			color = "96, 96, 96", 			value=0,		position = Settings.PresetINTENSITYStartPosition+Settings.PresetGridWidth*2},
		
		{name = "Strobe Fast", 			color = "255, 255, 0", 			value=nil,		position = Settings.PresetINTENSITYStartPosition+1},
		{name = "Strobe Mid", 			color = "255, 255, 128", 		value=nil,		position = Settings.PresetINTENSITYStartPosition+Settings.PresetGridWidth+1},
		{name = "Strobe Low", 			color = "255, 255, 204", 		value=nil,		position = Settings.PresetINTENSITYStartPosition+Settings.PresetGridWidth*2+1},
		
		{name = "Shutter Open", 		color = "255, 255, 255", 		value=nil,		position = Settings.PresetINTENSITYStartPosition+2},
		{name = "Shutter Closed", 		color = "0, 0, 0", 				value=nil,		position = Settings.PresetINTENSITYStartPosition+Settings.PresetGridWidth+2}
	}
	Spb8Colors = {
		{name = "White", 				color = "255, 255, 255", 							position = Settings.PresetCOLORStartPosition,
			value={Red=255,		Green=255,		Blue=255,	White=255,		Amber=0,		Uv=0,		Cyan=0,		Magenta=0,		Yellow=0}},

		{name = "Red", 					color = "255, 0, 0", 								position = Settings.PresetCOLORStartPosition+1,
			value={Red=255,		Green=0,		Blue=0,		White=0,		Amber=0,		Uv=0,		Cyan=0,		Magenta=255,	Yellow=255}},

		{name = "Orange", 				color = "255, 127, 0", 								position = Settings.PresetCOLORStartPosition+2,
			value={Red=255,		Green=127,		Blue=0,		White=0,		Amber=0,		Uv=0,		Cyan=0,		Magenta=127,	Yellow=255}},

		{name = "Yellow", 				color = "255, 255, 0", 								position = Settings.PresetCOLORStartPosition+3,
			value={Red=255,		Green=255,		Blue=0,		White=0,		Amber=0,		Uv=0,		Cyan=0,		Magenta=0,		Yellow=255}},

		{name = "CTO", 					color = "255, 191, 127", 							position = Settings.PresetCOLORStartPosition+4,
			value={Red=255,		Green=191,		Blue=127,	White=255,		Amber=255,		Uv=0,		Cyan=0,		Magenta=64,		Yellow=128}},
			
		{name = "Green", 				color = "0, 255, 0", 								position = Settings.PresetCOLORStartPosition+5,
			value={Red=0,		Green=255,		Blue=0,		White=0,		Amber=0,		Uv=0,		Cyan=255,	Magenta=0,		Yellow=255}},

		{name = "CTB", 					color = "200, 200, 200", 							position = Settings.PresetCOLORStartPosition+6,
			value={Red=191,		Green=191,		Blue=255,	White=255,		Amber=0,		Uv=0,		Cyan=64,	Magenta=64,	Yellow=0}},	

		{name = "Cyan", 				color = "0, 255, 255", 								position = Settings.PresetCOLORStartPosition+7,
			value={Red=0,		Green=255,		Blue=255,	White=0,		Amber=0,		Uv=0,		Cyan=255,	Magenta=0,		Yellow=0}},

		{name = "Blue", 				color = "0, 0, 255", 								position = Settings.PresetCOLORStartPosition+8,
			value={Red=0,		Green=0,		Blue=255,	White=0,		Amber=0,		Uv=0,		Cyan=255,	Magenta=255,	Yellow=0}},

		{name = "Uv", 					color = "64, 0, 127", 								position = Settings.PresetCOLORStartPosition+9,
			value={Red=64,		Green=0,		Blue=191,	White=0,		Amber=0,		Uv=255,		Cyan=191,	Magenta=255,	Yellow=64}},

		{name = "Pink", 				color = "255, 127, 127", 							position = Settings.PresetCOLORStartPosition+10,
			value={Red=255,		Green=127,		Blue=127,	White=0,		Amber=0,		Uv=0,		Cyan=0,		Magenta=127,	Yellow=127}},

		{name = "Magenta", 				color = "255, 0, 255", 								position = Settings.PresetCOLORStartPosition+11,
			value={Red=255,		Green=0,		Blue=255,	White=0,		Amber=0,		Uv=0,		Cyan=0,		Magenta=255,	Yellow=0}}
	}
	Spb8Beams = {
		{name = "No prism", 			color = "0, 0, 0", 				value=nil,			position = Settings.PresetBEAMStartPosition},
		{name = "Prism Rot CW Slow", 	color = "255, 0, 0", 			value=nil,			position = Settings.PresetBEAMStartPosition+Settings.PresetGridWidth},
		{name = "Prism Rot CW Fast", 	color = "255, 255, 0", 			value=nil,			position = Settings.PresetBEAMStartPosition+Settings.PresetGridWidth*2},
		{name = "Prism fixed", 			color = "160, 160, 160", 		value=nil,			position = Settings.PresetBEAMStartPosition+1},
		{name = "Prism Rot CCW Slow", 	color = "0, 0, 255", 			value=nil,			position = Settings.PresetBEAMStartPosition+Settings.PresetGridWidth+1},
		{name = "Prism Rot CCW Fast", 	color = "0, 255, 255", 			value=nil,			position = Settings.PresetBEAMStartPosition+Settings.PresetGridWidth*2+1},
		
		{name = "Focus Near", 			color = "0, 0, 0", 				value={Focus=0},			position = Settings.PresetBEAMStartPosition+2},
		{name = "Focus Middle", 		color = "160, 160, 160", 		value={Focus=127},			position = Settings.PresetBEAMStartPosition+Settings.PresetGridWidth+2},
		{name = "Focus Far", 			color = "255, 0, 0", 			value={Focus=255},			position = Settings.PresetBEAMStartPosition+Settings.PresetGridWidth*2+2},
		
		{name = "Frost 100%", 			color = "255, 255, 255",		value={Frost=255},			position = Settings.PresetBEAMStartPosition+3},
		{name = "Frost 50%", 			color = "160, 160, 160", 		value={Frost=127},			position = Settings.PresetBEAMStartPosition+Settings.PresetGridWidth+3},
		{name = "Frost 0%", 			color = "96, 96, 96", 			value={Frost=0},			position = Settings.PresetBEAMStartPosition+Settings.PresetGridWidth*2+3}
	}
	
----------------------------------------------------
-- Main Script - dont change if you don't need to --
----------------------------------------------------

	----------------------
	-- Global Variables --
	----------------------
	
	_Debug_ = false  -- Advanced log for debug purpose only, do not use in production
	
	ScriptInfos = {
		version = "1.2",
		name = "Auto Presets",
		author = "Sylvain Guiblain",
		contact = "sylvain.guiblain@gmail.com",
		website = "http://spb8.lighting"
	}
	
	EN = {
		Common = {
			Actions 				= "What do you want to do?",
			Create 					= "Create Auto Presets",
			Populate 				= "Populate Auto Presets",
			Remove 					= "Remove Auto Presets",
			Cuelist 				= "Create Cuelist for Color Auto Presets",
			CuelistPrefix 			= "Please indicate the GROUP NAME",
			GroupSelect				= "Please indicate the GROUP ID: ",
			SelectFixture			= "Please select your fixture(s) and validate by \"Yes\"",
			Is16Bits				= "Is your fixture set on 16 bits?",
			AnswerYes 				= "==>YES",
			AnswerNo 				= "==>No",
			InProgress 				= "Processing",
			CreationNothingDone		= "==>No presets generated",
			CreationQuestion		= "Do you want to create auto VAR presets?",
			CreationDone			= "Auto VAR presets created",
			Done					= "auto presets threated",
			RemoveNothingDone		= "==>No presets removed",
			RemoveQuestion			= "Do you want to remove auto VAR presets?",
			RemoveDone				= "auto VAR presets removed",
			Removed					= "auto presets removed",
			PopulateNothingDone		= "==>No presets populated",
			PopulateQuestion		= "Do you want to populate auto VAR presets?",
			PopulateDone			= "auto VAR presets populated",
			Populated				= "auto presets populated",
			Escape					= "Escape"
		},
		ToPlayback = {
			GroupSelected			= "Group VAR selected",
			ApplyingPreset			= "Applying preset: VAR",
			CreatingCuelist			= "Creating cuelist: VAR",
			ApplyStyle				= "Applying some style",
			CopyCuelist 			= "Copying cuelist to playback"
		},
		Infos = {
			OutofScope				= "Nothing done",
			ScriptBy				= "Scripted by"
		}
	}
	
	FR = {
		Common = {
			Actions 				= "Que voulez-vous faire ?",
			Create 					= "Créer des Presets Auto",
			Populate 				= "Peupler les Presets Auto",
			Remove 					= "Supprimer les Presets Auto",
			Cuelist 				= "Créer une Cuelist pour chaque Presets Auto de couleur",
			CuelistPrefix 			= "Veuillez indiquer le NOM DU GROUPE",
			GroupSelect				= "Veuillez indiquer l'ID DU GROUPE: ",
			SelectFixture			= "Veuillez sélectionner vos machines et valider par \"Oui\"",
			Is16Bits				= "Est-ce que votre machine est en 16 bits?",
			AnswerYes 				= "==>OUI",
			AnswerNo 				= "==>NON",
			InProgress 				= "Traitement en cours",
			CreationNothingDone		= "==>Pas de presets généré",
			CreationQuestion		= "Voulez-vous créer des Presets Auto de VAR ?",
			CreationDone			= "Presets Auto de VAR créés",
			Done					= "Presets Auto générés",
			RemoveNothingDone		= "==>Aucun preset supprimé",
			RemoveQuestion			= "Voulez-vous supprimer les Presets Auto de VAR ?",
			RemoveDone				= "Presets Auto de VAR supprimés",
			Removed					= "Presets Auto supprimés",
			PopulateNothingDone		= "==>Aucun preset peuplé",
			PopulateQuestion		= "Voulez-vous peupler les Presets Auto de VAR ?",
			PopulateDone			= "Presets Auto de VAR peuplés",
			Populated				= "Presets Auto peuplés",
			Escape					= "Quitter"
		},
		ToPlayback = {
			GroupSelected			= "Groupe VAR sélectionné",
			ApplyingPreset			= "Preset VAR appliqué",
			CreatingCuelist			= "Cuelist VAR créé",
			ApplyStyle				= "Application de style",
			CopyCuelist 			= "Cuelist copié vers Playback"
		},
		Infos = {
			OutofScope				= "Rien n'a été fait",
			ScriptBy				= "Écris par"
		}
	}
	-- Set the language
	if Settings.Language == "EN" then
		Spb8String = EN
	elseif Settings.Language == "FR" then
		Spb8String = FR
	else
		Spb8String = EN
	end

	Spb8Types = {
		"Intensity",
		"Color",
		"Beam"
	}
	Spb8Counters = {
		Global = 0,
		PlayBack = 1
	}
	Spb8Answers = {
		PopulateDimmer = false,
		Is16bitsDimmer = false,
		PopulateColor = false,
		Is16bitsColor = false,
		PopulateBeam = false,
		Is16bitsBeam = false
	}
		
	----------------------
	-- Global Functions --
	----------------------
	
	function Debug(str)
		if _Debug_ then
			print("Debug - " .. str)
		end
	end
	function Replace(sentence, str)
		return string.gsub(sentence, "VAR", str)
	end
	function SetPreset(sType, sValue, Manual, On16bits)
		if Manual ~= true then
			ClearProgrammer()
			-- Select Fixture 1 Through 99999
				Key("Num1")
				Key("Thru")
				Key("Num9")
				Key("Num9")
				Key("Num9")
				Key("Num9")
				Key("Num9")
				Key("Enter")
			-- Add a small timer to allow right creation
		end
		Sleep(Settings.WaitTime)
		-- Set the preset with some content
		if sType == Spb8Types[2] or sType == Spb8Types[3] then -- If Colors or Beams
			for key, value in pairs(sValue) do
			if On16bits then
				value = value * 257
			end
				SetCVVal(key, value, true)
				Debug("SetCVVal(\"" .. key .. "\", " .. value .. ", true)")
			end
		else -- Others
			SetCVVal(sType, sValue, true)
			Debug("SetCVVal(\"" .. sType .. "\", " .. sValue .. ", true)")
		end
	end
	
	function EmptyPreset(sType, sAction)
	
		-- Catch configuration objects following the requesting action
		if sType == Spb8Types[1] then
			sPresets = Spb8Intensities
		elseif sType == Spb8Types[2] then
			sPresets = Spb8Colors
		elseif sType == Spb8Types[3] then
			sPresets = Spb8Beams
		else
			do return end
		end
		
		-- Adapt "generic question" to dedicated one with Preset Type name
		if sAction == 0 then
			StringQuestion = Replace(Spb8String.Common.CreationQuestion, sType)
		elseif sAction == 1 then
			StringQuestion = Replace(Spb8String.Common.PopulateQuestion, sType)
		elseif sAction == 2 then
			StringQuestion = Replace(Spb8String.Common.RemoveQuestion, sType)
		end
		
		Counter = 0
		
		-- Request to the EU if he want to create preset for each Preset Type		
		if PromptYesNo(StringQuestion, "")	then
			ClearProgrammer()
			
			-- Adapt "generic text string" to dedicated one with Preset Type name
			if sAction == 0 then
				StringDone = Replace(Spb8String.Common.CreationDone, sType)
			elseif sAction == 1 then
				StringDone = Replace(Spb8String.Common.PopulateDone, sType)
			elseif sAction == 2 then
				StringDone = Replace(Spb8String.Common.RemoveDone, sType)
			end
			
				print(StringQuestion .. "\r\n\t" .. Spb8String.Common.AnswerYes .. "\r\n\t" .. Spb8String.Common.InProgress) -- Log

			-- For each preset, recursivily do some interesting stuff ...
			for i,sPreset in pairs(sPresets) do
			 ----------------------
			-- Create or Poputate --
			 ----------------------
				if sAction == 0 or sAction == 1 then
					if sType == Spb8Types[1] then
						if sPreset.value ~= nil and sAction == 1 then -- If preset has default value for populate, and the action is populate
							if Spb8Answers.PopulateDimmer ~= true then -- If the question has not been yet asked to get some EU details
								Spb8Answers.PopulateDimmer = PromptYesNo(Spb8String.Common.SelectFixture, "") -- Ask EU to provide a fixture selection (can be group or direct selection)
								Spb8Answers.Is16bitsDimmer = PromptYesNo(Spb8String.Common.Is16Bits, "") -- Ask EU to indicate if its fixture is 16bits resolution or not for this preset
							end
							if Spb8Answers.PopulateDimmer then -- If populate details has been provided
								SetPreset(Spb8Types[1], sPreset.value, true, Spb8Answers.Is16bitsDimmer) -- Populate function runned!
							end
						end
						if sPreset.value ~= nil and sAction == 1 or sAction == 0 then
							RecordIntensityPreset(sPreset.position, sPreset.name, true) -- Once steps before are achieved, don't bin it, record it :)
						end
					elseif sType == Spb8Types[2] then
						if sPreset.value ~= nil and sAction == 1 then -- If preset has default value for populate, and the action is populate
							if Spb8Answers.PopulateColor ~= true then -- If the question has not been yet asked to get some EU details
								Spb8Answers.PopulateColor = PromptYesNo(Spb8String.Common.SelectFixture, "") -- Ask EU to provide a fixture selection (can be group or direct selection)
								Spb8Answers.Is16bitsColor = PromptYesNo(Spb8String.Common.Is16Bits, "") -- Ask EU to indicate if its fixture is 16bits resolution or not for this preset
							end
							if Spb8Answers.PopulateColor then -- If populate details has been provided
							SetPreset(Spb8Types[2], sPreset.value, true, Spb8Answers.Is16bitsColor) -- Populate function runned!
							end
						end
						if sPreset.value ~= nil and sAction == 1 or sAction == 0 then
							RecordColorPreset(sPreset.position, sPreset.name, true) -- Once steps before are achieved, don't bin it, record it :)
						end
					elseif sType == Spb8Types[3] then
						if sPreset.value ~= nil and sAction == 1 then -- If preset has default value for populate, and the action is populate
							if Spb8Answers.PopulateBeam ~= true then -- If the question has not been yet asked to get some EU details
								Spb8Answers.PopulateBeam = PromptYesNo(Spb8String.Common.SelectFixture, "") -- Ask EU to provide a fixture selection (can be group or direct selection)
								Spb8Answers.Is16bitsBeam = PromptYesNo(Spb8String.Common.Is16Bits, "") -- Ask EU to indicate if its fixture is 16bits resolution or not for this preset
							end
							if Spb8Answers.PopulateBeam then -- If populate details has been provided
								SetPreset(Spb8Types[3], sPreset.value, true, Spb8Answers.Is16bitsBeam) -- Populate function runned!
							end
						end
						if sPreset.value ~= nil and sAction == 1 or sAction == 0 then
							RecordBeamPreset(sPreset.position, sPreset.name, true) -- Once steps before are achieved, don't bin it, record it :)
						end
					end
					PresetName = sPreset.name -- Define the Preset Name to be used for Log and Notification purpose
			 ----------------------
			--       Remove       --
			 ----------------------
				elseif sAction == 2 then
					if sType == Spb8Types[1] then
						PresetName = GetIntensityPresetName(sPreset.position)
						DeleteIntensityPreset(sPreset.position)
					elseif sType == Spb8Types[2] then
						PresetName = GetColorPresetName(sPreset.position)
						DeleteColorPreset(sPreset.position)
					elseif sType == Spb8Types[3] then
						PresetName = GetBeamPresetName(sPreset.position)
						DeleteBeamPreset(sPreset.position)
					end
				end
				-- Some specific actions for logging activity
					if PresetName ~= "" then
						print("\t\t" .. PresetName) -- Log
						Counter = Counter + 1
						Spb8Counters.Global = Spb8Counters.Global + 1
					end
			end
				-- Once done, advertise EU with a so nice notification in M-PC
				ShowNotification(sType, Counter .. " " .. StringDone)
				print("\t" .. Counter .. " " .. StringDone .. "\n") -- Log
			ClearProgrammer()
		else
			-- Track the EU answer to avoid any issue later on ... Joking for sure x-)
			print(StringQuestion .. "\r\n\t" .. Spb8String.Common.AnswerNo) -- Log
		end
		return true
	end
	
	----------------------
	--        RUN       --
	----------------------
	ChoiceNumber = ""
	
	::START::
	
	print(ScriptInfos.name .. " v" .. ScriptInfos.version) -- Log
	
	-- Prepare the complete text option, it will not fit the pop-up ...
	print(Spb8String.Common.Actions)
	Sentence = "0 - " .. Spb8String.Common.Create .. "\r\n" .. "1 - " .. Spb8String.Common.Populate .. "\r\n" .. "2 - " .. Spb8String.Common.Remove .. "\r\n" .. "3 - " .. Spb8String.Common.Cuelist .. "\r\n" .. "5 - " .. Spb8String.Common.Escape
	
	-- Request what to do to EU
	WhatToDo = Prompt(Sentence, ChoiceNumber)
	if WhatToDo ~= "" then -- If not empty, means EU want to do something
		WhatToDo = tonumber(WhatToDo)
	else -- Else, EU want to exit
		print("\t" .. Spb8String.Infos.OutofScope)
		goto END
	end
	
	-- Simple switch to run the right function and control the input, never be too confident with EU inputs ^^	
	if WhatToDo <= 2 then
		for i,Spb8Type in pairs(Spb8Types) do
			if WhatToDo == 0 then
				EmptyPreset(Spb8Type, 0)
			elseif WhatToDo == 1 then
				EmptyPreset(Spb8Type, 1)
			elseif WhatToDo == 2 then
				EmptyPreset(Spb8Type, 2)
			end
		end
	-- Other proposition, is to integrate the ColorPresets2PlaybackButtons adapted to the color preset creation
	elseif WhatToDo == 3 then
		-- Request Group ID
		GroupID = tonumber(Prompt(Spb8String.Common.GroupSelect, ""))
		CueNamePrefix = Prompt(Spb8String.Common.CuelistPrefix, "")
			
		for i,sColor in pairs(Spb8Colors) do
			
			-- Select Group and presets and record cuelist in the cuelist directory
			SelectGroup(GroupID) 
				print("\t" .. Replace(Spb8String.ToPlayback.GroupSelected, GroupID)) -- Log
			SelectColorPreset(sColor.position)
				print("\t" .. Replace(Spb8String.ToPlayback.ApplyingPreset, sColor.name)) -- Log
			CueListName = CueNamePrefix .. " - " .. sColor.name
			CuelistNumber = RecordCuelistOnDirectory(CueListName)
				print("\t" .. Replace(Spb8String.ToPlayback.CreatingCuelist, CueListName)) -- Log
			SetCuelistAppearance(CuelistNumber, GetColorPresetAppearance(sColor.position))
				print("\t" .. Spb8String.ToPlayback.ApplyStyle) -- Log
			
			ClearProgrammer()
			
			-- Assign cuelist to the playback button
			CopyCuelistFromDirectoryToPlaybackButton(CuelistNumber, Settings.PlaybackButtonPage, Spb8Counters.PlayBack)
				print("\t" .. Spb8String.ToPlayback.CopyCuelist) -- Log
			
			Spb8Counters.PlayBack = Spb8Counters.PlayBack + 1
		end
	else
		print("\t" .. Spb8String.Infos.OutofScope)
		goto END
	end
	
	----------------------
	--       STATS      --
	----------------------
	
	-- Display some global stats at the end of the script
	if Spb8Counters.Global == 0 then
		if WhatToDo == 0 then
			print("\n" .. Spb8String.Common.CreationNothingDone .. "\n") -- Log
		elseif WhatToDo == 1 then
			print("\n" .. Spb8String.Common.PopulateNothingDone .. "\n") -- Log
		elseif WhatToDo == 2 then
			print("\n" .. Spb8String.Common.RemoveNothingDone .. "\n") -- Log
		end
	else
			print("\n==> " .. Spb8Counters.Global .. " " .. Spb8String.Common.Done .. "\n") -- Log
	end
	
	----------------------
	--       INFOS      --
	----------------------
	
	if WhatToDo then
		ChoiceNumber = WhatToDo + 1
	end

	goto START
	
	::END::
	
	print(ScriptInfos.name .. " v" .. ScriptInfos.version .. "\r\n" .. Spb8String.Infos.ScriptBy .. "\r\n\t" .. ScriptInfos.author .. "\r\n\t" .. ScriptInfos.contact .. "\r\n\t" .. ScriptInfos.website) -- Log