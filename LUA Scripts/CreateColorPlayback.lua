-- CreateColorPlayback
-- 		Author: Sylvain Guiblain <sylvain.guiblain@gmail.com> http://spb8.lighting
--
-- This script will create playback cuelist from color preset

-------------------
-- Configuration --
-------------------

Settings = {
	WaitTime = 0.5
	}
	
----------------------------------------------------
-- Main Script - dont change if you don't need to --
----------------------------------------------------

ScriptInfos = {
		version = "1.1",
		name = "CreateColorPlayback",
		author = "Sylvain Guiblain",
		contact = "sylvain.guiblain@gmail.com",
		website = "http://spb8.lighting"
	}
	
print(ScriptInfos.name .. " v" .. ScriptInfos.version) -- Log

FinalMessage = ""

function RecordCuelist(number)
	Key("Record")
		Sleep(Settings.WaitTime)
	Key("Slash")
		Sleep(Settings.WaitTime)
	Key("Slash")
	if string.find(number, '%d', 1, false) then
		a = string.match(number, "(.+)")
		for c in a:gmatch"." do
			Key("Num"..c)
		end
		
		Key("Enter")
	else
	end
	Sleep(Settings.WaitTime)
	Key("Enter")
end

-- Request EU number of groups to be threated
NbOfGroups 		= tonumber(Prompt("Number of Groups:", ""))		-- Indicate the number of Groups to be threated
Groups = {}

-- Request EU details for each group
for i=1,NbOfGroups,1 do 
	GroupName 		= Prompt("Group n°".. i .." name:", "")		-- Request the Group Name
	GroupID 		= tonumber(Prompt("Group n°".. i .." ID:", ""))		-- Request the Group ID
	table.insert(Groups, {name = GroupName, id = GroupID})	-- Create a line in the groups
end

	-- Feedback to EU about group declaration
	FinalMessage = NbOfGroups .. " groups declared:" .. "\n"
	print(NbOfGroups .. " groups declared:")
	for i, Group in pairs(Groups) do
		FinalMessage = FinalMessage .. "\t" .. "- n°" .. Group.id .. " " .. Group.name .. "\n"
		print("\t" .. "- n°" .. Group.id .. " " .. Group.name)
	end

-- Request EU the preset number to be threated
PresetColorFirst 	= tonumber(Prompt("First Color Preset:", ""))		-- First color preset
PresetColorLast 	= tonumber(Prompt("Last Color Preset:", ""))		-- Last color preset

-- Extract Preset Name, ID and Appearance
ColorPresets = {}
for i=PresetColorFirst,PresetColorLast,1 do
	table.insert(ColorPresets, {name = GetColorPresetName(i), id = i, appearance = GetColorPresetAppearance(i)})
end
Settings.NumberOfCOLORPreset = PresetColorLast - PresetColorFirst + 1

	-- Feedback to EU about color preset
	FinalMessage = FinalMessage .. Settings.NumberOfCOLORPreset .. " Color Presets (from n°" .. PresetColorFirst .. " to n°" .. PresetColorLast .. ")" .. "\n"
	print(Settings.NumberOfCOLORPreset .. " Color Presets (from n°" .. PresetColorFirst .. " to n°" .. PresetColorLast .. "):")
	for i, ColorPreset in pairs(ColorPresets) do
		FinalMessage = FinalMessage .. "\t" .. "- n°" .. ColorPreset.id .. " " .. ColorPreset.name .. " " .. ColorPreset.appearance .. "\n"
		print("\t" .. "- n°" .. ColorPreset.id .. " " .. ColorPreset.name .. " " .. ColorPreset.appearance)
	end

-- Request EU informations about playback
Settings.PlaybackButtonPage 	= tonumber(Prompt("Playback Button Page:", "1"))	-- Starting playback button page
Settings.PlaybackButtonStart 	= tonumber(Prompt("Playback Button Start:", "1"))	-- First playback button

	-- Feedback to EU about playback
	FinalMessage = FinalMessage .. "Playback options:" .. "\n"
	print("Playback options:")
		FinalMessage = FinalMessage .. "\t" .. "- Playback page n°" .. Settings.PlaybackButtonPage .. "\n"
		print("\t" .. "- Playback page n°" .. Settings.PlaybackButtonPage)
		FinalMessage = FinalMessage .. "\t" .. "- Start from Playback button n°" .. Settings.PlaybackButtonStart .. "\n"
		print("\t" .. "- Start from Playback button n°" .. Settings.PlaybackButtonStart)

-- Request EU the first Cuelist Number to record
Settings.StartingEmptyCueList 	= tonumber(Prompt("First Cuelist To Record To:", ""))
Settings.FadeTime 	= tonumber(Prompt("Cue Fade Time:", "0.1"))
Settings.ReleaseTime 	= tonumber(Prompt("Cuelist Release Time:", "0"))

	-- Feedback to EU about playback
	FinalMessage = FinalMessage .. "Record options:" .. "\n"
	print("Record options:")
		FinalMessage = FinalMessage .. "\t" .. "- new Cuelist from n°" .. Settings.StartingEmptyCueList .. "\n"
		print("\t" .. "- new Cuelist from n°" .. Settings.StartingEmptyCueList)
		FinalMessage = FinalMessage .. "\t" .. "- Cue Fade Time: " .. Settings.FadeTime .. "s" .. "\n"
		print("\t" .. "- Cue Fade Time: " .. Settings.FadeTime .. "s")
		FinalMessage = FinalMessage .. "\t" .. "- Cuelist Release Time: " .. Settings.ReleaseTime .. "s" .. "\n"
		print("\t" .. "- Cuelist Release Time: " .. Settings.ReleaseTime .. "s")
		
FinalMessage = FinalMessage .. "\t" .. "- Cuelist Release Time: " .. Settings.ReleaseTime .. "s" .. "\n"

YesOrNo	= PromptYesNo("Do you agree to generate Playbacks Color for " .. NbOfGroups .. "x" .. Settings.NumberOfCOLORPreset .. " grid size on Playback page n°" .. Settings.PlaybackButtonPage .. "?" .. "\n\n" .. FinalMessage, "")

Counter = {
	Cuelist = Settings.StartingEmptyCueList,
	PlaybackNumber = Settings.PlaybackButtonStart
}

if YesOrNo == true then
	for i, Group in pairs(Groups) do
		--¨For each color preset
		for i, ColorPreset in pairs(ColorPresets) do
			ClearProgrammer()
					Sleep(Settings.WaitTime)
						SelectGroup(Group.id)
					Sleep(Settings.WaitTime)
						SelectColorPreset(ColorPreset.id)
					Sleep(Settings.WaitTime)
						RecordCuelist(Counter.Cuelist)
					Sleep(Settings.WaitTime)
						CopyCuelistFromDirectoryToPlaybackButton(Counter.Cuelist, Settings.PlaybackButtonPage, Counter.PlaybackNumber)
					Sleep(Settings.WaitTime)
						SelectCuelist(Counter.Cuelist)
					Sleep(Settings.WaitTime)
						SetSelectedCuelistName(Group.name .. ' - ' .. ColorPreset.name)
					Sleep(Settings.WaitTime)
						SetCuelistAppearance(Counter.Cuelist, ColorPreset.appearance)	-- Apply the color preset appearance to the cuelist
					Sleep(Settings.WaitTime)
						RenameCue(1, ColorPreset.name)
					Sleep(Settings.WaitTime)	
						SetCueFadeTime(1, Settings.FadeTime)
					Sleep(Settings.WaitTime)
						SetCuelistReleaseTime(Counter.Cuelist, Settings.ReleaseTime)
				Counter.Cuelist = Counter.Cuelist + 1													-- Go to next cuelist
				Counter.PlaybackNumber = Counter.PlaybackNumber + NbOfGroups	-- Set the next position
		end
		Counter.PlaybackNumber = Settings.PlaybackButtonStart + i
	end
ShowNotification("Creation finished!", ScriptInfos.name .. " v" .. ScriptInfos.version .. "\r\n" .. "Scripted by" .. "\r\n\t" .. ScriptInfos.author .. "\r\n\t" .. ScriptInfos.contact .. "\r\n\t" .. ScriptInfos.website)
else
	print("Nothing performed as requested!")
end

print("\r\n" .. ScriptInfos.name .. " v" .. ScriptInfos.version .. "\r\n" .. "Scripted by" .. "\r\n\t" .. ScriptInfos.author .. "\r\n\t" .. ScriptInfos.contact .. "\r\n\t" .. ScriptInfos.website) 
	