-- Reset Fade
-- 		Author: Sylvain Guiblain <sylvain.guiblain@gmail.com> http://spb8.lighting
--
-- Allow to reset the fade to 0

-------------------
-- Configuration --
-------------------

Cuelist = {}
Cuelist.CLStart 	= tonumber(Prompt("CL Start:", ""))
Cuelist.CLEnd		= tonumber(Prompt("CL End:", ""))
Cuelist.TimeFade 		= tonumber(Prompt("Cue Fade Time:", ""))
Cuelist.CueStart 	= tonumber(Prompt("Cue Start:", ""))
Cuelist.CueEnd		= tonumber(Prompt("Cue End:", ""))
Cuelist.TimeRelease		= tonumber(Prompt("Cuelist Release Time:", ""))

Settings = {
	WaitTime = 0.5
	}

----------------------------------------------------
-- Main Script - dont change if you don't need to --
----------------------------------------------------

for CL = Cuelist.CLStart, Cuelist.CLEnd do
	SelectCuelist(CL)
	Sleep(Settings.WaitTime)
	for ActCue = Cuelist.CueStart, Cuelist.CueEnd do
	 SetCueFadeTime(ActCue, Cuelist.TimeFade)
	 Sleep(Settings.WaitTime)
	end
	SetCuelistReleaseTime(CL, Cuelist.TimeRelease)
	Sleep(Settings.WaitTime)
end