-- 	Color Presets to Playback Buttons
-- 		Author: Ricardo Dias <ricdays@gmail.com> Extended By: Nadav Yarkoni <Nadav@Yarkoni.co.il>
--
-- This script creates a sequence of buttons in the playback button
-- grid for the selected group with preset name and appearance, and a 
-- single cue containing the group with the preset recorded

-------------------
-- Configuration --
-------------------



fixtureGroupNumber 		= tonumber(Prompt("Group Number:", ""))		-- The fixture group to be used
CueNamePrefix			= Prompt("Prefix for cuelist names:", "") 	-- Prefix, Optional, for naming all the cues
CueNameSuffix			= Prompt("Suffix for cuelist names:", "") 			-- Suffix, Optional, for naming all the cues.

presetNumberStart 		= tonumber(Prompt("Preset Number Start:", ""))		-- First color preset to be copied to playback buttons
presetNumberEnd 		= tonumber(Prompt("Preset Number End:", ""))		-- Last color preset to be copied to playback buttons

playbackButtonPage 		= tonumber(Prompt("Playback Button Page:", ""))		-- Starting playback button page
playbackButtonStart 	= tonumber(Prompt("Playback Button Start:", ""))	-- First playback button

----------------------------------------------------
-- Main Script - dont change if you don't need to --
----------------------------------------------------

ClearProgrammer(); -- Make sure the programmer is cleared

counter = 1
for presetNumber = presetNumberStart, presetNumberEnd do

	-- Select fixtures and presets and record cuelist in the cuelist directory
	SelectGroup(fixtureGroupNumber)
	SelectColorPreset(presetNumber)
	cuelistName = CueNamePrefix .. " " .. GetColorPresetName(presetNumber) .. " " .. CueNameSuffix
	cuelistNumber = RecordCuelistOnDirectory(cuelistName)
	SetCuelistAppearance(cuelistNumber, GetColorPresetAppearance(presetNumber));
	
	print(cuelistName)
	
	ClearProgrammer()

	-- Assign cuelist to the playback button
	pbkBtnNum = playbackButtonStart - 1 + counter
	CopyCuelistFromDirectoryToPlaybackButton(cuelistNumber,playbackButtonPage,pbkBtnNum)

	counter = counter + 1
end

ClearProgrammer(); -- Make sure the programmer is cleared in the end